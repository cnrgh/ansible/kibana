# Kibana role

An [Ansible](https://www.ansible.com/) role for installing [Kibana](https://www.elastic.co/kibana)
on distributions based on [RHEL](https://fr.wikipedia.org/wiki/Red_Hat_Enterprise_Linux).

## Role variables

### Installation

- `kb_version`: Kibana version to install (default: `latest`),
- `kb_pkg_file`: Name of the Kibana package file (default: `kibana-{{ kb_version }}-x86_64.rpm`),
- `kb_pkg_url`: URL to download the Kibana package from
  (default: `https://artifacts.elastic.co/downloads/kibana/{{ kb_pkg_file }}`),
- `kb_inst_log`: File where the Kibana installation stdout will be written
  (default: `"{{ ansible_env.HOME }}/kb_inst_stdout.log"`).

### Configuration

- `kb_port`: Kibana port (default: `5601`),
- `kb_ssl`: Activate SSL by setting `server.ssl.enabled` to true in `kibana.yml` (default: `false`),
  > Currently, required SSL parameters `server.ssl.certificate` and `server.ssl.key`
  > are not automatically configured by the role, thus preventing the Kibana server from starting successfully.
- `kb_wait_until_ready`: Wait for the Kibana server to have fully started before
  moving on to the next task (default: `false`).
- `kb_availability_check_retries`: Control the maximum number of retries
  when `kb_wait_until_ready` is `true` (default: `30`),
- `kb_availability_check_delay`: Control the delay between retries
  when `kb_wait_until_ready` is `true` (default: `10`),
- `kb_es_hosts`: List of Elasticsearch instance URLs used for Kibana queries (default: `[]`),
- `kb_es_token_value`: Value of the service account token used by Kibana to authenticate to Elasticsearch.
- `kb_es_http_ca_path`: Path to an Elasticsearch Certificate Authority (CA) file to copy to the remote host.
  If an empty path is provided,
  `elasticsearch.ssl.certificateAuthorities` will not be set and
  `elasticsearch.ssl.verificationMode` will be set to `none` (default: `""`).
