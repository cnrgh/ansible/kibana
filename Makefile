all:

check:
	ansible-lint -svv .

docker:
	docker build -t ansible-elasticsearch .

.PHONY: all check