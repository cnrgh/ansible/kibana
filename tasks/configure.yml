---
- name: Set permissions for Kibana directories.
  ansible.builtin.file:
    path: "{{ item.path }}"
    owner: kibana
    group: kibana
    mode: "{{ item.mode }}"
    state: directory
    recurse: true
  loop:
    - {path: "/usr/share/kibana", mode: "0755"}  # home, bin and plugins
    - {path: "/etc/kibana", mode: "0750"}  # config
    - {path: "/var/log/kibana", mode: "0755"}  # logs
    - {path: "/var/lib/kibana", mode: "0755"}  # data
  become: true

- name: Ensure the 'certs/' directory exists in Kibana config.
  ansible.builtin.file:
    path: "{{ _config_dir }}certs/"
    state: directory
    owner: kibana
    group: kibana
    mode: "0750"
  become: true

- name: Copy the Elasticsearch certificate to the 'certs/' directory.
  ansible.builtin.copy:
    src: "{{ kb_es_http_ca_path }}"
    dest: "{{ _config_dir }}certs/ca.crt"
    owner: kibana
    group: kibana
    mode: "0750"
  become: true
  # Copy the CA only when a path is given.
  when: kb_es_http_ca_path | length > 0

- name: Determine the Elasticsearch certificate verification mode.
  ansible.builtin.set_fact:
    ssl_cert_verification_mode: "{{ 'full' if kb_es_http_ca_path | length > 0 else 'none' }}"

- name: Set the Elasticsearch certificate verification mode.
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^(# *)?elasticsearch.ssl.verificationMode: .*$'
    line: >-
      elasticsearch.ssl.verificationMode: "{{ ssl_cert_verification_mode }}"
  become: true

- name: Set the Elasticsearch certificate path.
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^(# *)?elasticsearch.ssl.certificateAuthorities: .*$'
    line: >-
      elasticsearch.ssl.certificateAuthorities: [ "{{ _config_dir }}certs/ca.crt" ]
  become: true
  when: kb_es_http_ca_path | length > 0

- name: Disable telemetry.
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^#?telemetry.enabled: .*$'
    line: 'telemetry.enabled: false'
  become: true

- name: Set server port
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^#?server.port: .*$'
    line: 'server.port: {{ kb_port }}'
  become: true

- name: Set IP address for network connection
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^#?server.host: .*$'
    line: 'server.host: {{ ansible_host }}'
  become: true

- name: Enable/disable SSL
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^#?server.ssl.enabled: .*$'
    line: 'server.ssl.enabled: {{ kb_ssl }}'
  become: true

- name: Set Elasticsearch hosts
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^#?elasticsearch.hosts: .*$'
    line: 'elasticsearch.hosts: [{% for host in kb_es_hosts %}{% if not loop.first %}, {% endif %}"{{ host }}"{% endfor %}]'
  become: true

- name: Set Elasticsearch service account token
  ansible.builtin.lineinfile:
    path: /etc/kibana/kibana.yml
    regexp: '^(# *)?elasticsearch.serviceAccountToken: .*$'
    line: 'elasticsearch.serviceAccountToken: "{{ kb_es_token_value }}"'
  become: true
